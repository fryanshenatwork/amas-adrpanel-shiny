---
output: 
  html_document:
    mathjax: local
    self_contained: false
    width: 8.2677
    hight: 11.69289
    css: topdf.css
    toc_float: TRUE
    keep_md: TRUE
template: default
#params:
#  da: NA
#  s: NA
#  t1: NA
#  t2: NA
#  Th: NA
#  Hy: NA
#  Lu: NA
#  Pr: NA
---

```{r include=FALSE}
load("/srv/shiny-server/FRuiDesign_backend/variable.RData")
load("/srv/shiny-server/FRuiDesign_backend/outputSelect.RData")
source("/srv/shiny-server/FRuiDesign_backend/function.R")
library(Hmisc)
dataAll<-variableAll$da
```

<style>
@media print { 
@page {
    size: A4 !important;
    display: -webkit-flex;
    margin-right: -70px;
    <!-- margin: 0 -20px 0 -20px!important; -->
    <!-- margin: -30px; -->
	
    pageWidth :8.2677!important;
    pageHeight :11.69289!important;
}
div[title=ADR]{
position: absolute;
left:0px;
width:100%!important;
margin: 0;
background-color: #0B4C8E !important; 
-webkit-print-color-adjust: exact;
}
div[title=ADR] b {
color: #FFFFFF!important;
-webkit-print-color-adjust: exact;
}
div[title=ADR] sup {color: #FFFFFF!important;
-webkit-print-color-adjust: exact;
}
div[title=ADR] em {
color: #FFFFFF!important;
-webkit-print-color-adjust: exact;
}
div[title=ADR] p1 {
color: #FFFFFF!important;
-webkit-print-color-adjust: exact;
}
div[title=bg]{
position: absolute;
left:0px;
top:83px;
width:100%;
background-color: #2A85D5!important;
padding-top: 10px!important;
-webkit-print-color-adjust: exact;
}
div[title=blank]{
position: absolute;
left:0px;
top:80px;
width:100% !important;
padding-top: 3px!important;

}
div[title=plot]{
position: relative;
left:0px!important;
padding-left: 0px !important;
padding-right:-30px !important;
margin-right:-30px!important;
top:178px !important;
width: 210mm !important;
}
div[title=plot] h {
font-size: 20px;
position: relative;
padding-top: 10px!important;
padding-left: 10px!important;
color: #3666AB !important;
-webkit-print-color-adjust: exact;

}
div[title=plot1]{
position: relative;
text-align: center!important;
}
div[title=plot2]{
position: relative;
text-align: center!important;
top:300px;
}

div[title=plot2] b {
position: relative;
padding-top: 0px!important;
padding-bottom: 0px!important;
top:10px;
left:46%!important;
font-size:16px;
text-align: center!important;
-webkit-print-color-adjust: exact;
}
div[title=highchart]{
position: relative;
top:-70px;
height: 100%!important;
width:100%!important;
left:30px;
padding-top: 0px!important;
padding-bottom: 0px!important;

-webkit-print-color-adjust: exact;
}
div[title=highchart1]{
position: absolute;
top:300px;
height: 100%!important;
width:100%!important;
left:30px;
padding-top: 0px!important;
padding-bottom: 0px!important;

-webkit-print-color-adjust: exact;
}
div[title=corr]{
position: relative;
top:50px!important;
}
div[title=corr] h {
font-size: 20px;
position: relative;
padding-top: 10px!important;
padding-left: 10px!important;
padding-bottom: 10px!important;
color: #3666AB !important;
-webkit-print-color-adjust: exact;
}
div[title=corrplotT]{
margin-right: 60px!important;
position: relative;
top:50px;
left:45px!important;
margin-bottom:auto!important;

}
div[title=corrplot1]{
padding-left:0px;
padding-top: 0px;
padding-right:0px;
padding-bottom: 0px;
text-align: left;
font-size: 14px;
margin:0;
position: absolute;
top:10px !important;
bottom: 20px!important;
left:30px!important;
hight: 500px!important;
width: 100%!important;
<!-- background:rgb(248, 251, 252)!important; -->
-webkit-print-color-adjust: exact;
}
div[title=corrplot11]{
text-align:left;
font-size: 14px;
padding-left:0px;
padding-top: 0px;
padding-right:0px;
padding-bottom: 0px;
margin:0;
position: relative;
top:80px !important;
left:15px!important;
bottom: 20px!important;
hight: 30px!important;
width: 120px!important;
<!-- background:rgb(248, 251, 252)!important; -->
-webkit-print-color-adjust: exact;
}
div[title=corrplot12]{
text-align:left;
font-size: 14px;
padding-left:0px;
padding-top: 0px;
padding-right:0px;
padding-bottom: 0px;
margin:0;
position: absolute;
top:150px !important;
left:30px!important;
bottom: 20px!important;
hight: 30px!important;
width: 100%!important;
<!-- background:rgb(248, 251, 252)!important; -->
-webkit-print-color-adjust: exact;
}
div[title=corrplot2]{
text-align:left;
font-size: 14px;
padding-left:0px;
padding-top: 0px;
padding-right:0px;
padding-bottom: 0px;
margin:0;
position: absolute;
top:10px !important;
left:100px!important;
bottom: 20px!important;
hight: 30px!important;
width: 140px!important;
<!-- background:rgb(248, 251, 252)!important; -->
-webkit-print-color-adjust: exact;

}
div[title=corrplot21]{
text-align:left;
font-size: 14px;
padding-left:0px;
padding-top: 0px;
padding-right:0px;
padding-bottom: 0px;
margin:0;
position: relative;
top:80px !important;
bottom: 20px!important;
left:85px!important;
hight: 30px!important;
width: 140px!important;
<!-- background:rgb(248, 251, 252)!important; -->
-webkit-print-color-adjust: exact;

}
div[title=corrplot22]{
text-align:left;
font-size: 14px;
padding-left:0px;
padding-top: 0px;
padding-right:0px;
padding-bottom: 0px;
margin:0;
position: absolute;
top:150px !important;
bottom: 20px!important;
left:100px!important;
hight: 30px!important;
width: 100%!important;
<!-- background:rgb(248, 251, 252)!important; -->
-webkit-print-color-adjust: exact;

}

.myplot {
    position: relative;
    top:-75px !important;
    left:440px !important;
    width: 350px;
    height: 300px;
    overflow: hidden!important;
}

.myplot img {
    width: 350px;
    height: 350px;
    margin: -35px 0px -100px -20px;
    overflow: hidden!important;
}

html,body{
    height:11.69289!important;
    width:8.2677!important;
    margin: 0!important;
    left:0px!important;ssssss
}
}
   <body>
        <script type="text/javascript">window.status = 'ready';</script>
    </body>
</style>

<div title="ADR" class="header">
  <p><b>AMAS Cloud<sup><em>　Data Analysis Report</em><p1>　資料分析表</p1></sup></b></p>
</div>
<div title="blank" class="header2">
</div>
<div title="bg" class="header3">
</div>

<div style="font-size:15px;">
<!-- # ```{r global_options, R.options=knitr::opts_chunk$set(warning=FALSE, message=FALSE)} -->
<!-- # ```    -->

```{r echo=FALSE, results="asis",fig.width=12,fig.height=3,context="setup"}
# a<-paste0(params$t1," - ",params$t2)
# a<-paste0("2017-11-23"," ~ ","2017-11-24")
# header<-c("User","Data Source","Report Type","Time Interval")
# dataH<-data.frame("wewe","http://scada.amastek.com.tw/proj/HY.html","Weekly Report",a)
# colnames(dataH)<-header
# # dataH

tableD<-renderTable({
  # a<-paste0(params$t1," ~ ",params$t2)
  t<-paste0(variableAll$t1," ~ ",variableAll$t2)
  # a<-paste0("2017/11/23"," ~ ","2017/11/24")
  header<-c("User","Project Name","Report Type","Time Interval")
  dataH<-data.frame(capitalize(paste0(variableAll$user_name)),paste0(variableAll$proj_name),paste0(variableAll$datatype," Report"),t)
  colnames(dataH)<-header
  xtable(dataH, auto = TRUE)
  # print(xtable(dataH), type="html")
  })
# 
fluidRow(
        shiny::HTML("<div style='padding-top: 20px;'>"),
         shiny::HTML("<div style='padding-right: 10px; padding-bottom: 10px;position: absolute;top:95px;width:100%;'>"),
          column(10,shiny::HTML(tableD()))
)


# tableD()
# print(kable(dataH)
#       %>%
#    kable_styling(bootstrap_options = "striped", full_width = T, position = "float_right")


```

<p style="border-style: solid;border-width: 2px;font-size: 30px;padding-top: 10px;padding-left: 10px;position: absolute;
top:100px;left:680px;padding-bottom: 10px;padding-right: 10px;text-align: center;">`r floor((variableAll$nc-2)/3)+2`/`r floor((variableAll$nc-2)/3)+2`</p>
</div>
<div title="plot" class="outputS">
<h>資料圖表</h>
<div title="plot1">
<p><b>`r variableAll$colName[length(variableAll$colName)]`<b></p>
</div>
<div title="highchart">
```{r echo=FALSE,fig.width=12,fig.height=3}
# Pressure(dataAll,sum)
#Pr<-variableAll$Pr
Pr<-plotOutput[length(plotOutput)]
htmltools::tagList(Pr)

Sys.sleep(0.01)
```
</div>
</div>
<div title="corr">
<h>資料相關性分析</h>
</div>
<div title="corrplotT">
```{r echo=FALSE, fig.height=3, fig.width=6}

 #table2<-renderTable({
 # corrD<-as.data.frame(CorrPlot(dataAll))
 # header<-"溫度 vs 濕度"
 # dataT<-data.frame(corrD$Thermometer[1])
 # colnames(dataT)<-header
 # head(xtable(dataT, auto = TRUE))
 #},align = 'l')
table2<-renderTable({
corrD<-as.data.frame(CorrPlot(dataAll[,c("Time",variableAll$VSelect[1],variableAll$VSelect[2],variableAll$VSelect[3])]))
  header<-paste0( variableAll$colName[1]," vs ",variableAll$colName[2])
  dataT<-data.frame(corrD[1,variableAll$VSelect[2]])
  colnames(dataT)<-header
  head(xtable(dataT, auto = TRUE))
 },align = 'l')
table21<-renderTable({
  #corrD<-as.data.frame(CorrPlot(dataAll))
  corrD<-as.data.frame(CorrPlot(dataAll[,c("Time",variableAll$VSelect[1],variableAll$VSelect[2],variableAll$VSelect[3])]))
  #header<-"濕度 vs 照度"
  header<-paste0( variableAll$colName[2]," vs ",variableAll$colName[3])
  #dataT<-data.frame(corrD$Hygrometer[2])
  dataT<-data.frame(corrD[2,variableAll$VSelect[3]])
  colnames(dataT)<-header
  head(xtable(dataT, auto = TRUE))
 },align = 'l')
table3<-renderTable({
  #corrD<-as.data.frame(CorrPlot(dataAll))
  corrD<-as.data.frame(CorrPlot(dataAll[,c("Time",variableAll$VSelect[1],variableAll$VSelect[2],variableAll$VSelect[3])]))
  #header<-"溫度 vs 照度"
  header<-paste0( variableAll$colName[1]," vs ",variableAll$colName[3])
  #dataT1<-data.frame(corrD$Thermometer[2])
  dataT1<-data.frame(corrD[1,variableAll$VSelect[3]])
  colnames(dataT1)<-header
  head(xtable(dataT1, auto = TRUE))
},align = 'l')
table31<-renderTable({
  corrD<-as.data.frame(CorrPlot(dataAll))
  header<-"濕度 vs 大氣壓力"
  dataT1<-data.frame(corrD$Hygrometer[4])
colnames(dataT1)<-header
  head(xtable(dataT1, auto = TRUE))
},align = 'l')
table4<-renderTable({
  corrD<-as.data.frame(CorrPlot(dataAll))
  header<-"溫度 vs 大氣壓力"
  dataT3<-data.frame(corrD$Thermometer[4])
  colnames(dataT3)<-header
  head(xtable(dataT3, auto = TRUE))
},align = 'l')
table41<-renderTable({
  corrD<-as.data.frame(CorrPlot(dataAll))
  header<-"照度 vs 大氣壓力"
  dataT3<-data.frame(corrD$Luminometer[4])
  colnames(dataT3)<-header
  xtable(dataT3,type="html")
  # htmlTable(dataT3, css.tspanner = "text-align: left;")
},align = 'l')
#fluidRow(
#  column(width = 4,tags$div(title="corrplot1" ,
#         shiny::HTML(table2())),tags$div(title="corrplot11" ,shiny::HTML(table3())),tags$div(title="corrplot12" ,shiny::HTML(table4())
#  # ,tags$head( type="text/css",tags$style("#table4 table {padding-left: 50px;text-align: left;}"))
#  )),
#  # ,shiny::HTML("</div>")
#  column(width = 4,tags$div(title="corrplot2",
#         shiny::HTML(table21())),tags$div(title="corrplot21",shiny::HTML(table31())),tags$div(title="corrplot22",shiny::HTML(table41())))
#)

fluidRow(
  column(width = 4,tags$div(title="corrplot1" ,
         shiny::HTML(table2())),tags$div(title="corrplot11" ,shiny::HTML(table3()))),

  column(width = 4,tags$div(title="corrplot2",
         shiny::HTML(table21())))
)
```
</div>
 <div class="myplot">
 <img src="corrplot1.jpg" alt="Paris">
 </div>


