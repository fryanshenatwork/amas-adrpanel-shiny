function vueSumm(){
  var appSumm = new Vue({
    el: '#app-summ',
    data: {
      shinySummOverview: null,
      shinySummCorrelation: null
    },
    methods: {
      updateFromShiny: function (type, val) {
        if (type && val) {
          this[type] = val
        } else {
          this[bindKey.shinySummOverview] = window[bindKey.shinyTable]['sum']
          this[bindKey.shinySummCorrelation] = window[bindKey.shinyTable]['corrtable']
        }
        return true
      }
    },
    mounted:function () {
      this.updateFromShiny()
    },
    updated: function () {
      $('#summOverview')
        .tablesorter({
          widthFixed: true
        })

      $('#summCorr')
        .tablesorter({
          widthFixed: true
        })
    }
  })

  window.appSumm = appSumm
}