function vuePlot() {
  var appPlot = new Vue({
    el: '#app-plot',
    data: {
      plotTypes: window.shinyDataInit['types'],
      plotRange: {
        start: moment(window.shinyDataInit['range'][0], 'YYYY-MM-DD').format('YYYY/MM/DD'),
        end: moment(window.shinyDataInit['range'][1], 'YYYY-MM-DD').format('YYYY/MM/DD')
      },
      plotRangePrint: null,
      plotType: window.shinyDataInit['types'][0],
      plotSplitdata: 'true'
    },
    methods: {
      plotTypeUpdate: function (event) {
        // console.log(event)
        bData(bindKey['plotType'], event.target.value)
      },
      plotDownloadExisting: function (event) {
        // bData(bindKey.plotDownloadExisting, true)
        $('#plotDownloadExisting').click()
      },
      plotDownloadNew: function (event) {
        var checkedList = []
        $('[name=download-columns]:checked').each(function (){
          checkedList.push($(this).val())
        })
        if (checkedList.length > 1) {
          bData(bindKey.plotDownloadNew, {
            outputColumns: checkedList
          })
        } else {
          $(event.target).tooltip({
            title: 'Require two columns at least',
            trigger: 'manually',
            placement: 'bottom'
          })
          $(event.target).tooltip('show')
          setTimeout(() => {
            $(event.target).tooltip('hide')
          }, 1500);
          // null
        }
      },
      changeRangeType: function (event) {
        // console.log(event.target.value)
        this.datePicker(this.plotRangeType);
        bData(bindKey['plotSelectType'], this.plotRangeType, false)
      }
    },
    mounted: function () {
      bData(bindKey['plotRange'],
        {
          start: this.plotRange['end'],
          end: this.plotRange['end']
        }
      )
      bData(bindKey['plotType'], this.plotType);
      // plotSplitdata init

      // Call collapseListen
      collapseListen()

      // tooltips
      $('#amas-util-split').tooltip({
        title: 'If "NO" it will take some time to loading data.',
        trigger: 'hover focus',
        placement: 'bottom'
      })

      // hide shiny btn
      $('#plotDownloadExisting').hide()

    }
  })
}

function collapseListen(){
  // Collapse Function
  var switchButton = '#btn-plot-toggle'
  var accordion = '.plot-accordion'

  $(switchButton).on('click', function () {
    var now = Number($(this).attr('data-at'))
    now = now + 1
    if (now > 2) { now = 1 }
    // console.log(now)
    plotCollapse(now)
    $(this).attr('data-at', now)
  })

  function plotCollapse(item) {
    var target = $('.panel:nth-child(' + item + ') .panel-title a', accordion)
    target[0].click()
  }

  // download switch collapse
  $('[name="dl-type"]').on('click', function () {
    var target = $($(this).attr('data-target'))
    var isOpen = target.hasClass('in')
    if (isOpen == false) {
      // console.log('action')
      $('.download-collapse > div').collapse('hide')
      target.collapse('show')
    }
  })
}
