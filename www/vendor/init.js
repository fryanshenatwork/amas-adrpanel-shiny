// =============
// Shiny Init
// =============
var bindKey = {
  plotType: 'dataType',
  plotRange: 'dataRange',
  plotDownloadExisting: 'plotDownloadExisting',
  plotDownloadNew: 'plotDownloadNew',
  plotSplitdata: 'plotSplitdata',
  plotSelectType: 'plotSelectType',
  shinyTable: 'shinyTable',
  shinyDataInit: 'shinyDataInit',
  shinySummOverview: 'shinySummOverview',
  shinySummCorrelation: 'shinySummCorrelation',
  shinyData: 'shinyData'
}

$(document).on("shiny:sessioninitialized", function(event) {
  rwdNavbar();

  Shiny.addCustomMessageHandler("shinyDataInit", function (val) {
    // console.log('ShinyDataInit called', val);
    window[bindKey.shinyDataInit] = val['val'];
    vueProjects();
    amasUtilModal('hide')
  });

  Shiny.addCustomMessageHandler('shinyUpdateTable', function (val) {
    window[bindKey.shinyTable] = val['val']

    // >>> console
    amasConsole(val, 'recive')

    var st = true
    if (window.appData) {
      st = appData.updateFromShiny()
    }
    if (window.appSumm) {
      st = appSumm.updateFromShiny()
    }

    if (st == true) {
      amasUtilModal('hide')
    }
    // console.log(val['val'])
    // appSumm.updateFromShiny(bindKey.shinySummOverview, val['val']['sum']);
    // appSumm.updateFromShiny(bindKey.shinySummCorrelation, val['val']['corrtable']);
  })

  Shiny.addCustomMessageHandler('shinyCallBack', function (val) {
    amasUtilModal('hide')
  })

  function vueProjects () {
    vuePlot()
    vueInit()
    // vueSumm()
  }
});

// =============
// Header collapse function
// =============
function rwdNavbar() {
  var el = 'nav.navbar.navbar-default'
  var button = '<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\" ><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button>'

  $('#adr', el).wrap('<div id=\"navbar\" class=\"navbar-collapse collapse\"></div>')
  $('.navbar-header', el).append(button)
}

// =============
// vueInit
// =============
function vueInit() {
  // vueInit Using components "theFilter"
  $('.navbar-header')
    .parent('.container-fluid')
    .append('<thefilter /><amasutilmodal />')

  var appInit = new Vue ({
    el: 'nav.navbar',
    components: {
      'thefilter': httpVueLoader('components/filter.vue'),
      'amasutilmodal': httpVueLoader('components/modal.vue')
    },
    methods: {
      modal: function (st) {
        amasConsole('modal.vue called ' + st)
        el = $('#amas-util-modal')
        if (st == 'show' || st == null || st == '' || st == undefined){
          el.modal({
            show: true,
            backdrop: 'static'
          })
        } else {
          el.modal('hide')
        }
      }
    }
  })

  window.appInit = appInit
} // vueInit()


// =============
// common Utils
// =============
function pageSwitch (page) {
  $('[data-value=' + page + ']', 'ul#adr.nav').click();
} // pageSwitch

function amasConsole (msg, color) {
  color = color || 'black'
  var title = 'Console'
  // blue 3B568C // red D91A1A
  switch (color) {
    case 'send':
    color = '#508365'; title = 'Send   '; break;
    case 'recive':
    color = '#ECA414'; title = 'Recive '; break;
    case 'log':
    color = '#3B568C'; title = 'Log    '; break;
    default:
    color = color
  }

  var style = '; color: white; text-align: center; border-radius: .3em; padding: 0 .3em;'

  console.log('%c' + title, 'background-color:' + color + style, msg)
} //amasConsole

function amasUtilModal (st) {
  // >>>
  amasConsole('AmasUtilModal Status: ' + st, 'log')
  appInit.modal(st)
} //amasUtilModal