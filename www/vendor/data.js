function vueData() {
  var appData = new Vue({
    el: '#app-data',
    data: {
      datacompleteData: null
    },
    methods: {
      updateFromShiny: function (val) {
        if (val) {
          this.datacompleteData = val
        } else {
          this.datacompleteData = window[bindKey.shinyTable]['dataAll']
          // console.log(this.datacompleteData)
        }
        return true
      }
    },
    mounted: function () {
      this.updateFromShiny()
    },
    updated: function () {
      var em = $('#dataComplete')
      em.trigger('destroyPager')
      em.trigger('updateAll')

      em.tablesorter({
        widthFixed: true
      }).tablesorterPager({
        pageReset: true,
        container: $('#dataCompletePager'),
        gotoPage: '.gotoPage'
      })

    }
  })

  window.appData = appData
}
